# React Lost in Translation WebApp
This project is a sign language translator created with the React library. The reason for the creation of this project is that it was an assignment at the Noroff Accelerated .NET Fullstack course.

## The Application
The application is a sign language translator which translates typed words and sentences to sign language using pictures of each letter. The application first asks you to log in with a username. If a new username is typed it will create an account and log you in. To translate you simply need to type what you want to translate in the input box and each letter will then be translated to sign language. Hit 'Save' to save your translation on your profile.

The navigation bar on the top shows the different pages you have access to. Press 'Profile' to navigate to the profile page. Here your last ten translations will show together with an input field where you can edit your username. To delete the translation history, simply press 'Delete translation history'.

The application is hosted in azure on the link: https://lostintranslation-sondre-haavard.azurewebsites.net/

## How to run the program
You can also host the program yourself on localhost after cloning the repository.The program uses npm to run, check it out if you havent installed it yet.To run the program, simply write 'npm start' in the IDE console and the application will open in the web browser.

## Contributors:
- Sondre Eftedal: @SondreEftedal
- Håvard Madland: @havardmad
