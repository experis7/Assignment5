import { NavLink } from "react-router-dom"
import { useUser } from "../../contexts/UserProvider"
import { storageSave } from "../../utils/Storage"


function Navbar(){
    //Gets the setter from the context.
    const {setUser} = useUser()

    //Function to handle the logout page. Sets the context and the session storage user to null.
    function handleLogoutClick(){
        if(window.confirm('Are you sure you want to log out?')){
            storageSave('user',null)
            setUser(null)
            
        }
    }
    return(
        <nav>
            <ul>
                <li className="NavbarLogo">
                    <img src={require('../../Logo.png')} alt="Logo" height="40"></img>
                    <span>Lost in translation</span>
                </li>
                <li>
                    <NavLink onClick={handleLogoutClick}>Log out</NavLink>;
                </li>
                <li>
                    <NavLink to="/profile">Profile</NavLink>;
                </li>
                <li>
                    <NavLink to="/translation">Translation</NavLink>;
                </li>
                
                
            </ul>
            
        </nav>
    )
}
export default Navbar