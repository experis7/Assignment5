import {useState} from 'react'
import { addTranslation } from '../../api/translation'
import { useUser } from '../../contexts/UserProvider'
import { storageSave } from '../../utils/Storage'
import TranslationImages from './TranslationImages'


function TranslationForm(){
    //State of the string that the user wants to translate
    const [translate,setString] = useState({
        value: ''
    }
    )
    //Getter and setter for the user context.
    const { user , setUser } = useUser()

    //Function to handle the submit button on the form. Calls the function that handles the API call to add a new translation. If no error message is returned, it updates the session storage and the user context.
    const onSubmit = async event => {
        event.preventDefault()
        
        TranslationImages(translate.value)
        const [error,result] = await addTranslation(translate.value, user)
        if(error === null){
            storageSave('user', result)
            setUser(result)
        }
        else{
            window.alert(error.message)
        }    
    }

    //Handles the change of the translation string. Updates the state when the input changes.
    const handleChange = event => {
        event.preventDefault()
        setString({value: event.target.value})    
    }

  
 
    return(
        <>
        <h2>Please enter what you want to translate</h2>
        <div className='TranslationForm'>
            <form onSubmit={onSubmit}>
                <input type="textbox"
                placeholder='Sentence'
                onChange={handleChange}/>
                
                 
            
            <button type="submit">Save</button>
            </form>
        </div>
        <div className='ImageView'>
            <TranslationImages word={translate.value}/>
        </div>
        
        </>
    )

}
export default TranslationForm