

//Function to display the images from the translation string. Uses string manipulation to give each character in the string path to the connected image for the word. Also uses regex to exclude the special characters. Then it pushes the urls to an array of the urls. That will be used to set the src in the img tags.
function TranslationImages({word}){
    const images = []
    const regex = /[a-zA-Z]/
    for(let i in word){
        if(regex.test(word[i]) === true){
            images.push(<img src ={"/signs/"+word[i].toLowerCase() +".png"} alt={"image of handsign" +word[i].toLowerCase()}/>)  
        }
        else if(word[i] === ' '){
            images.push(<img src="/signs/space.png" alt={word[i]}></img>)
        }
        else{
            console.log("Cannot print that special character")
        }
        
    }

      return(
        <>
            {images}
        </>
    )
}  
export default TranslationImages

