import {  useState } from "react"
import { updateUsername } from "../../api/user"
import React from "react"
import { useUser } from "../../contexts/UserProvider"
import { removeTranslations } from "../../api/translation"
import { storageSave } from "../../utils/Storage"

function UserInformation(){
    //Get the getter and setter for the context user
    const {user, setUser} = useUser()
    
    
    //State for the username
    const[username,setUsername] = useState({
        value: user.username
    })
    
    //Variable that contains a list of the 10 latest translation from the user.
    let translationList =user.translations.slice(-10).map((item,index)=>{
        return <li key={index}>{item}</li>
    })
    
 
    //Function to handle the submit of the call to update the username. Updates the user context and the session storage if no error message is returned.
    const onSubmit = async event => {
        event.preventDefault()
        const[error,data] = await updateUsername(username.value , user.id)
        if(error === null){
            storageSave(data)
            setUser(data)
        }
        else{
            window.alert(error.message)
        }            
    }

    //Function to handle the delete translation button click. Calls the function to send the api PATCH request and updates the session storage and context if no error message is returned.
    const onDelete = async event => {
        event.preventDefault()
        
        const[error,result] = await removeTranslations(user.id)
        if(error !== null){
            window.alert(error)
        }
        else{
            storageSave(result)
            setUser(result)
        }
    }
    //Handles the change of the input from the edit username field.
    const handleChange = event => {
        setUsername({
            ...username,
            value: event.target.value
          
        })
        console.log(username.value)
      }
    
    return(
        <>
        <div className="EditUser">
        <form>
            <h2>User information</h2>
            <label htmlFor="username">Edit Username:</label>
            <input type = "text" value={username.value} onChange={handleChange}/>
            
                <h3>Translations:</h3>
            <ul>
               {translationList}
            </ul>
            <div className="UserProfileButtons">
                <button type = "button"className="btn btn-danger" onClick={onDelete}>Delete translation history</button>
                <button type = "submit" className="btn btn-primary" onClick={onSubmit}>Save changes</button> 
            </div>    
        </form>
        </div>
        </>
    )
}
export default UserInformation