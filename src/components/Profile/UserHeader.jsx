function UserHeader({username}){
    return(
        <header className="UserHeader">
            <h3 >Hello, {username} 🍻</h3>
        </header>
    )
}
export default UserHeader