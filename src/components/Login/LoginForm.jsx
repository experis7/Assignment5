import { useEffect, useState} from 'react'
import { loginUser } from '../../api/user'
 
import React from 'react'
import {  useUser } from '../../contexts/UserProvider'
import {useNavigate} from 'react-router-dom'
import {  storageSave } from '../../utils/Storage'
import { useForm } from 'react-hook-form'


const usernameConfig ={
        required: true,
        minLength: 5,
    }


function LoginForm(){
    
    
    const {register,handleSubmit,formState: {errors}} = useForm()
    const navigate = useNavigate()
    const {user,setUser} = useUser()
    
    const[loading,setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)
    
    function errorMessage(){
        if(!errors.username){
            return null
        }
        if(errors.username.type === 'required'){
            return <span>Username is required</span>
        }
        if(errors.username.type === 'minLength'){
            return <span>The username must me 5 characters long</span>
        }
    }
    
        //Function to handle the form submit. It updates the storage and the context user if the API call is successfullS
        const onSubmit = async({username}) =>{
        setLoading(true)
            const [error, data] = await loginUser(username)
           
                if(error !== null){
                    setApiError(error)
                }
                if(data !== null){
                    console.log(data)
                    storageSave('user', data)
                    setUser(data)   
                }
        setLoading(false)
    }
    
    
    //Navigates the user to the translation view if the user is not null.
    useEffect(() => {
        if(user !== null){
            navigate('/translation')
        }
    }, [user, navigate])

    
 
    return(
        <>
        <div className='LoginFormHeader'>
                <div className='LoginFormHeaderText'>
                    <h2>Lost in translation</h2>
                    <h3>Get started</h3> 
                </div>
                <img src={require('../../Logo.png')} alt="Logo"></img>   
        </div>
        
        <div className='LoginForm'>
           <form onSubmit={handleSubmit(onSubmit)}>
                <input type="text"
                placeholder="What's your name?"
                {...register("username",usernameConfig)}
                />          
            <button type="submit" disabled={loading}>Continue</button>
            {errorMessage}
            {loading && <p>Loading....</p>}
            {apiError && <p>{apiError.message}</p>}
        </form> 
        </div>
        
        </>
    )

}
export default LoginForm
