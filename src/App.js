import './App.css';
import Navbar from './components/Navbar/Navbar';
import 'animate.css';


import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Login from './views/Login';
import Translation from './views/Translation';
import Profile from './views/Profile';

function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <Navbar/>
        <Routes>
          <Route path='/' element={ <Login /> } />
          <Route path="/translation" element={ <Translation /> } />
          <Route path="/profile" element={ <Profile /> } />
        </Routes>
    </div>
   </BrowserRouter>
  );
}

export default App;
