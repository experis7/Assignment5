
//Function to create the header that is needed to use some of the API endpoints.
const api_Key = process.env.REACT_APP_API_KEY
export function createHeaders() {
    const myHeader = new Headers()
    myHeader.append('Content-Type', 'application/json')
    myHeader.append('X-API-Key', api_Key)
    return myHeader

}
