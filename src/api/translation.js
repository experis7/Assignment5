
import { createHeaders } from "."


const apiURL = process.env.REACT_APP_API_URL

//Function to add a new translation. Returns an Array with potential response and error message if they exists
export async function addTranslation(translation,user){
    try{
      const response = await fetch(`${apiURL}/translations/${user.id}`, {
          method: 'PATCH', 
          headers: createHeaders(),
          body: JSON.stringify({
            translations: [...user.translations , translation]
          })
      })
      const data = await response.json()
      return [null,data]
    }
    catch(error){
      return [error.message, null]
    }
    
  }

//Function to add a new translation. Returns an Array with potential response and error message if they exists
    export async function removeTranslations(userId){
    try{
      const response = await fetch(`${apiURL}/translations/${userId}`, {
          method: 'PATCH', 
          headers: createHeaders(),
          body: JSON.stringify({
            translations: []
          })
      })
      const data = await response.json()
      return [null,data]
    }
    catch(error){
      return [error.message, null]
    }
    
  }