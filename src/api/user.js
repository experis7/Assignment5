import { createHeaders } from "."
const apiURL = process.env.REACT_APP_API_URL


//Function to check if a user exists by using a query for the username. The response is an array of users with the same username, so we used pop to select the first element since there should not be several users with the same username.
export async function checkForUser(username) {
    try{
    const response = await fetch(`${apiURL}/translations?username=${username}`,{
    headers : createHeaders()
    })
    const data = await response.json()
    return [null , data.pop()]
    }
    
    catch(error) {
        
        return [error.message,null]
    }
}

//Function to create a new user by sending a post request to the endpoint. Returns an array with data and error message if they exists.
async function createUser(username){
  try{
    const response = await fetch(`${apiURL}/translations`, {
    method: 'POST',
    headers: createHeaders(),
    body: JSON.stringify({ 
        username: username, 
        translations: [] 
    })
  })
  if(!response.ok){
    throw new Error("Could not create user: " + username)
  }
  const data = await response.json()
  return [null, data]
  }
  catch(error){
    return [error.message, []]
  }
  

}
//Function to log in a user. It first calls the check for user to check if there is a user with the entered username. Otherwise it creates a new user. Returns an array with error message and user if they exist.
export async function loginUser(username){
    const [error,user] = await checkForUser(username)
    if(error !== null){
      return [error.message,null]
    }
    if(user){
      return [null,user]
    }
      
      
    return await createUser(username)
    
}

//Function to handle the PATCH request to update a username. It returns an array with error message and the user if they exist.
export async function updateUsername(update,userId){
  try{
    console.log(JSON.stringify(update))
    const response = await fetch(`${apiURL}/translations/${userId}`, {
        method: 'PATCH', 
        headers: createHeaders(),
        body: JSON.stringify({
          username: update 
        })
    })
    const data = await response.json()
    return [null,data]
  }
  catch(error){
    return [error.message, null]
  }
  
}

