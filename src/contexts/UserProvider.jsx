import { createContext, useContext, useState } from "react"
import { storageRead } from "../utils/Storage"


export const UserContext = createContext()

export const useUser = () => {
  return useContext(UserContext)
}
function UserProvider({ children }) {

  //Initializing the user to the value from session storage. If the user is logged in, the application will redirect to the translation page. 
  const [user, setUser] = useState(storageRead('user'))

  //Specify the state
  const state = {
    user,
    setUser
  }

  return (
    <UserContext.Provider value={state}>
      {children}
    </UserContext.Provider>
  )
}

export default UserProvider
