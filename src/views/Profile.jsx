
import  UserInformation  from '../components/Profile/UserInformation'
import withAuth from '../hoc/withAuth'
import { useUser } from '../contexts/UserProvider'
import UserHeader from '../components/Profile/UserHeader'

function Profile(){
    
    //Profile view. Uses the higher order component for authentication
    const { user } = useUser()
    

    
    return(
        <>
        <UserHeader username = {user.username}/>
        <UserInformation user={user}/>
        </>
        
    )   
}
export default withAuth(Profile)