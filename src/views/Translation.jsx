import TranslationForm from "../components/Translation/TranslationForm"
import TranslationImages from "../components/Translation/TranslationImages"
import withAuth from "../hoc/withAuth"

//Translation view. Uses the higher order component for authenticating the user.
function Translation(){
    
    return(
        <div>
            <TranslationForm/>
            <TranslationImages></TranslationImages>
        </div>
        
    )
}
export default withAuth(Translation)