//Function to set the value of the session storage
export const storageSave = (key, value) => {
    sessionStorage.setItem(key,JSON.stringify(value))
}

//Function to read the value of the session storage.
export const storageRead = key =>{
    const data = sessionStorage.getItem(key)
    if(data){
        return JSON.parse(data)
    }

    return null
}